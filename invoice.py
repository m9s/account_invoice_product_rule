#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy

from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def __init__(self):
        super(Line, self).__init__()

        # Field product
        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []

        if '_parent_invoice.accounting_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.accounting_date']

        if '_parent_invoice.invoice_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.invoice_date']

        self._reset_columns()

    def on_change_product(self, vals):
        pool = Pool()
        date_obj = pool.get('ir.date')
        account_rule_obj = pool.get('account.account.rule')
        product_obj = pool.get('product.product')

        res = super(Line, self).on_change_product(vals)
        res['account'] = False
        res['account.rec_name'] = False
        account_type = 'expense'
        if vals.get('_parent_invoice.type') in (
                'out_invoice', 'out_credit_note'):
            account_type = 'revenue'
        party = vals.get('_parent_invoice.party')
        date = (vals.get('_parent_invoice.accounting_date')
            or vals.get('_parent_invoice.invoice_date')
            or date_obj.today())
        product = product_obj.browse(vals.get('product'))
        taxes = res.get('taxes')
        account = account_rule_obj.get_account(
            account_type, party, date, product, taxes)
        if account:
            res['account'] = account.id
            res['account.rec_name'] = account.rec_name
        return res

Line()
