# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Product Rule',
    'name_de_DE': 'Fakturierung Artikel Regel',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Rules in Account Invoice
    - Implements the use of rules for accounts defined on products
''',
    'description_de_DE': '''Kontenregeln in der Fakturierung
    - Implementiert die Nutzung von Kontenregeln in der Fakturierung
''',
    'depends': [
        'account_invoice',
        'account_product_rule'
    ],
    'xml': [
    ],
    'translation': [
    ],
}
